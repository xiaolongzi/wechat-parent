package com.pflm;

import com.alibaba.fastjson.JSONObject;
import com.pflm.module.template.service.TemplateService;
import com.pflm.utils.WeixinUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @author qinxuewu
 * @version 1.00
 * @time 14/11/2018上午 11:48
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class TemplateTests {

    @Autowired
    TemplateService templateService;


    @Test
    public void test1() throws Exception {
        String token="15_5L2fLqmRWIPc80tk61MIGlU8fPydaCbreLKrFEIzu6abuWmBrGlL3btbJGYGZ0KIf0na-Tu3K7b8s8YFPhsH2Iaf40_3KVjV1utlppHornkSDDDi0D_Qt9a-9kl27E0JGtt6ZVd2tqWBONR1NPGdAIAFHY";
        JSONObject res=templateService.getAllTemplate(token);
        System.err.println("请求结果*********"+res.toJSONString());
    }


    @Test
    public void test2() throws Exception {
        String token="15_3eUw7_vdPq-8PqrKoJszVrAiX_gylHFPyMXhmsacfgq7ynneloZPkY3hR_jfTgBLPYI4fGOTuZEJDeEiN2tmXFOvtTut0uYxdOrEUG3566Dw-eLGqNpRhdm6V9rkedN87Z6TfrGXya_Rq2cfOTWaAAAINQ";
        Map<String, Object> map = new HashMap<String, Object>();
        WeixinUtil.wxMsgMap(map, "first","111", "#173177");
        WeixinUtil.wxMsgMap(map, "accountType", "222", "#173177");
        WeixinUtil.wxMsgMap(map, "account", "33", "#173177");
        WeixinUtil.wxMsgMap(map, "amount", "444", "#173177");
        WeixinUtil.wxMsgMap(map, "result", "555", "#173177");
        WeixinUtil.wxMsgMap(map, "remark", "666", "#173177");
        JSONObject jso = new JSONObject();
        jso.put("touser", "oWslW6PvScXrGuELbdriuFWSILQY");
        jso.put("template_id", "13KTlTCWyWycoF-aXxb1_HcF83DwNEqtLsOH2g9KZlI");
        jso.put("url", "");
        jso.put("data", map);
        JSONObject res=templateService.send(token,jso);
        System.err.println("请求结果*********"+res.toJSONString());
    }



}
